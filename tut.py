import matplotlib
import requests

matplotlib.use('Agg')
import json
from datetime import date
import pytz
from datetime import datetime

datetime.utcnow().replace(tzinfo=pytz.utc)
from config import *
import joblib
import numpy as np
import pandas as pd
from keras import Sequential, optimizers
from keras.callbacks import CSVLogger, EarlyStopping, ModelCheckpoint, History
from keras.engine.saving import load_model
from keras.layers import LSTM, Dropout, Dense
from matplotlib import pyplot as plt
from pandas_datareader import data
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from tqdm import tqdm
import yfinance as yf


def crawl_today_data():
    '''
    http://quote.eastmoney.com/zs000001.html
    :return:
    '''
    datetime.utcnow().replace(tzinfo=pytz.utc)
    url = "http://push2.eastmoney.com/api/qt/stock/get?secid=1.000001&ut=bd1d9ddb04089700cf9c27f6f7426281&fields=f118,f107,f57,f58,f59,f152,f43,f169,f170,f46,f60,f44,f45,f168,f50,f47,f48,f49,f46,f169,f161,f117,f85,f47,f48,f163,f171,f113,f114,f115,f86,f117,f85,f119,f120,f121,f122&invt=2&cb=jQuery112402578907720154464_1576640625049&_=1576640625050"
    cont = requests.get(url)
    res_dict = json.loads(cont.text.split('(')[-1].split(')')[0])['data']
    High = res_dict['f44']
    Low = res_dict['f45']
    Open = res_dict['f46']
    Volume = res_dict['f48']
    Adj_Close = res_dict['f43']
    Close = res_dict['f43']
    df = pd.DataFrame({'High': High / 100,
                       'Low': Low / 100,
                       'Open': Open / 100,
                       'Volume': Volume / 1000000,
                       'Adj Close': Adj_Close / 100,
                       'Close': Close / 100
                       }, index=[date.today().isoformat()])
    panel_data = pd.read_csv(save_file, engine='python', index_col=0)
    if date.today().isoformat() in panel_data.index:
        panel_data.update(df)
    else:
        panel_data.append(df)
    content = panel_data.to_csv(index=True)
    with open(save_file, 'w') as sh_f:
        sh_f.write(content)
    print(df)
    return panel_data


def get_finace_data_from_yahoo(NAME_ID="000001.SS", start_date='1990-12-19', end_date='2019-12-16', save_file='sh.txt'):
    yf.pdr_override()
    # panel_data = data.DataReader(NAME_ID, "yahoo", start_date, end_date)
    panel_data = data.get_data_yahoo(NAME_ID, start_date, end_date, data_source='google')
    if verborse_print:
        print(panel_data.tail(9))
    all_weekdays = pd.date_range(start=start_date, end=end_date, freq='B')
    panel_data = panel_data.reindex(all_weekdays)
    panel_data = panel_data.fillna(method='ffill')
    panel_data = panel_data[panel_data['Volume'] <= 912345]  # filter out error data
    panel_data = panel_data[panel_data['Volume'] > 0]  # filter out error data
    content = panel_data.to_csv(index=True)
    with open(save_file, 'w') as sh_f:
        sh_f.write(content)
    panel_data = crawl_today_data()

    content = panel_data.tail(3).to_csv(index=True)
    with open(save_file.replace('.txt', '.tail.txt'), 'w') as sh_f:
        sh_f.write(content)
    if verborse_print:
        print(all_weekdays)

        print(panel_data.tail(10))

        print(panel_data.describe())

        print(panel_data.tail())

    return panel_data


def visual_raw_finace_data():
    panel_data = pd.read_csv(save_file, engine='python')
    plt.figure()
    plt.plot(panel_data["Open"])
    plt.plot(panel_data["High"])
    plt.plot(panel_data["Low"])
    plt.plot(panel_data["Close"])
    plt.title('GE stock price history')
    plt.ylabel('Price (USD)')
    plt.xlabel('Days')
    plt.legend(['Open', 'High', 'Low', 'Close'], loc='upper left')
    plt.savefig('static/stock_price_history.svg', format='svg')

    plt.figure()
    plt.plot(panel_data["Volume"])
    plt.title('GE stock volume history')
    plt.ylabel('Volume')
    plt.xlabel('Days')
    plt.savefig('static/stock_volume_history.svg', format='svg')

    if verborse_print:
        print("checking if any null values are present\n", panel_data.isna().sum())

    return 0


def build_timeseries(mat, y_col_index):
    # y_col_index is the index of column that would act as output column
    # total number of time-series samples would be len(mat) - TIME_STEPS
    dim_0 = mat.shape[0] - TIME_STEPS
    dim_1 = mat.shape[1]
    x = np.zeros((dim_0, TIME_STEPS, dim_1))
    y = np.zeros((dim_0,))

    for i in tqdm(range(dim_0)):
        x[i] = mat[i:TIME_STEPS + i]
        y[i] = mat[TIME_STEPS + i, y_col_index]
    if verborse_print:
        print("length of time-series i/o", x.shape, y.shape)
    return x, y


def trim_dataset(mat, batch_size):
    """
    trims dataset to a size that's divisible by BATCH_SIZE
    """
    no_of_rows_drop = mat.shape[0] % batch_size
    if (no_of_rows_drop > 0):
        return mat[:-no_of_rows_drop]
    else:
        return mat


def get_train_test_dataset(panel_data):
    if all_train:
        df_train, df_test = panel_data, panel_data
    else:
        df_train, df_test = train_test_split(panel_data, train_size=0.9, test_size=0.1, shuffle=False)
    if verborse_print:
        print("Train and Test size", len(df_train), len(df_test))
        print(df_train.tail(5))
        print(df_test.head(5))
    # scale the feature MinMax, build array
    x = df_train.loc[:, train_cols].values
    min_max_scaler = MinMaxScaler()
    x_train = min_max_scaler.fit_transform(x)
    x_test = min_max_scaler.transform(df_test.loc[:, train_cols])
    joblib.dump(min_max_scaler, 'min_max.scaler')
    x_t, y_t = build_timeseries(x_train, 3)
    x_t = trim_dataset(x_t, BATCH_SIZE)
    y_t = trim_dataset(y_t, BATCH_SIZE)
    x_temp, y_temp = build_timeseries(x_test, 3)
    x_val, x_test_t = np.split(trim_dataset(x_temp, BATCH_SIZE), 2)
    y_val, y_test_t = np.split(trim_dataset(y_temp, BATCH_SIZE), 2)

    return x_t, y_t, x_val, y_val, x_test_t, y_test_t


def build_model(x_t):
    lstm_model = Sequential()
    lstm_model.add(
        LSTM(100, batch_input_shape=(BATCH_SIZE, TIME_STEPS, x_t.shape[2]), dropout=0.0, recurrent_dropout=0.0,
             stateful=True, kernel_initializer='random_uniform'))
    lstm_model.add(Dropout(0.5))
    lstm_model.add(Dense(20, activation='relu'))
    lstm_model.add(Dense(1, activation='sigmoid'))
    optimizer = optimizers.RMSprop(lr=lr)
    lstm_model.compile(loss='mean_squared_error', optimizer=optimizer)

    return lstm_model


def train_model(model, save_file, x_t, y_t, x_val, y_val):
    history = History()
    checkpointer = ModelCheckpoint(filepath=save_file, monitor='val_loss', verbose=1, save_best_only=True)
    csv_logger = CSVLogger('log.log', append=True)
    early_stop = EarlyStopping(monitor='val_loss', min_delta=0, patience=10, verbose=0,
                               mode='auto', )
    model = load_model(saved_model_path)
    history_dict = model.fit(x_t, y_t, epochs=1000, verbose=2, batch_size=BATCH_SIZE,
                             shuffle=False, validation_data=(trim_dataset(x_val, BATCH_SIZE),
                                                             trim_dataset(y_val, BATCH_SIZE)),
                             callbacks=[csv_logger, early_stop, checkpointer, history])

    joblib.dump(history_dict, save_file.replace('.model', '.history'))

    return model, history


def test_and_visual(history, model, x_test_t, y_test_t):
    plt.figure()
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.legend(['train_loss', 'val_loss'])
    plt.savefig('static/loss_history.svg', format='svg')

    res = model.predict(trim_dataset(x_test_t, BATCH_SIZE), BATCH_SIZE)
    tru = trim_dataset(y_test_t, BATCH_SIZE)
    bingo = 0
    total = 0
    up = 0
    down = 0
    upp = 0
    downn = 0
    for item in range(1, len(res) - 1):
        total += 1
        if tru[item] - tru[item - 1] > 0:
            upp+=1
            if res[item] - res[item - 1] > 0:
                up += 1
                bingo += 1
        if tru[item] - tru[item - 1] < 0:
            downn+=1
            if res[item] - res[item - 1] < 0:
                down += 1
                bingo += 1

    plt.figure()
    plt.plot(res)
    plt.plot(trim_dataset(y_test_t, BATCH_SIZE))
    plt.legend(['pre', 'tru'])
    plt.title('up:%d upp:%d down:%d downn:%d bingo:%d total:%d' % (up,upp, down,downn, bingo, total))
    plt.savefig('static/pre.vs.tru.svg', format='svg')
    # plt.show()

    return 0


def predict_future(modle_path, data_file):
    model = load_model(modle_path)
    panel_data = pd.read_csv(data_file, engine='python')
    panel_data = panel_data.tail(20 * TIME_STEPS)
    min_max_scaler = joblib.load('min_max.scaler')
    x_test = min_max_scaler.transform(panel_data.loc[:, train_cols])
    x_t, y_t = build_timeseries(x_test, 3)
    # x_t = trim_dataset(x_t, BATCH_SIZE)
    res = model.predict(x_t, 20)
    if verborse_print:
        print(res[-5:])
    if res[-1] > res[-2]:
        up_down = "up"
    else:
        up_down = "down"

    par = (res[-1] - res[-2]) * 100 / res[-2]
    plt.figure()
    plt.plot(y_t[-60:])
    plt.plot(res[-61:])
    plt.legend(['tru', 'pre'])
    plt.savefig('static/future.svg', format='svg')
    # plt.show()
    return res, up_down, par


def update_tail(panel_data):
    tail_df = pd.read_csv(save_file.replace('.txt', '.tail.txt'))
    panel_data.update(tail_df)
    cont = panel_data.to_csv()
    with open(save_file, 'w') as s_f:
        s_f.write(cont)


def do_the_pipline():
    now_date = date.today().isoformat()

    #  get fiance price history
    if download:
        panel_data = get_finace_data_from_yahoo(NAME_ID=shanghai, start_date='2003-03-03', end_date=now_date,
                                                save_file=save_file)
        # input('plz recheck the tail file and replace with right data:')
        update_tail(panel_data)
        visual_raw_finace_data()
    panel_data = pd.read_csv(save_file, engine='python')
    # get train test valid set

    x_t, y_t, x_val, y_val, x_test_t, y_test_t = get_train_test_dataset(panel_data)

    # build model
    model = build_model(x_t)

    # train model
    if retrain:
        model, history = train_model(model, saved_model_path, x_t, y_t, x_val, y_val)
    else:
        model = load_model(saved_model_path)
        history = joblib.load(saved_model_path.replace('.model', '.history'))
    # test model and visual
    test_and_visual(history, model, x_test_t, y_test_t)

    # predict future
    res, up_down, par = predict_future(saved_model_path, save_file)

    with open('date.json', 'r') as d_l_f:
        cont = d_l_f.read()
        dict_cont = json.loads(cont)
    if now_date not in dict_cont.keys():
        dict_cont[now_date] = up_down + '    %.2f' % par
        with open('date.json', 'w') as d_l_f:
            d_l_f.write(json.dumps(dict_cont, indent=4))
    return up_down, par


if __name__ == '__main__':
    do_the_pipline()
