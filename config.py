# config settings
TIME_STEPS = 60
BATCH_SIZE = 20
lr = 0.000001
download = True
save_file = 'static/shh.txt'
retrain = False
all_train = True
saved_model_path = 'lstm.model'
train_cols = ["Open", "High", "Low", "Close", "Volume"]
shanghai = "000001.SS"
verborse_print = False
