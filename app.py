import json
from datetime import date
import pytz
from datetime import datetime
import pandas as pd

from config import save_file

datetime.utcnow().replace(tzinfo=pytz.utc)
from flask import Flask, render_template

from tut import do_the_pipline

app = Flask(__name__)


@app.route('/')
def index():
    now_date = date.today().isoformat()
    with open('date.json', 'r') as d_l_f:
        cont = d_l_f.read()
        dict_cont = json.loads(cont)
    if now_date not in dict_cont.keys():
        up_down, par = do_the_pipline()
        dict_cont[now_date] = up_down + '    %.2f' % par
        up_down = up_down + '    %.2f' % par
        with open('date.json', 'w') as d_l_f:
            d_l_f.write(json.dumps(dict_cont, indent=4))

    else:
        up_down = dict_cont[now_date]
    info = {'up_down': up_down}
    par = up_down.split(' ')[-1]
    if float(par) < 0:
        info['color'] = 'chartreuse'
    else:
        info['color'] = 'crimson'
    panel_data = pd.read_csv(save_file, engine='python')

    last_day = panel_data.tail(1)
    info['Open'] = last_day['Open'].values[0]
    info['High'] = last_day['High'].values[0]
    info['Low'] = last_day['Low'].values[0]
    info['Close'] = last_day['Close'].values[0]
    info['Volume'] = last_day['Volume'].values[0]
    info['Today'] = now_date
    return render_template('index.html', info=info)


if __name__ == '__main__':
    app.run('127.0.0.1', 1688, True)
